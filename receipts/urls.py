from django.urls import path
from receipts.views import receipts_list_view, create_receipts, expenses_view, accounts_view, create_category, create_account

urlpatterns = [
    path("", receipts_list_view, name="home"),
    path("create/", create_receipts, name="create_receipt"),
    path("accounts/", accounts_view, name="accounts"),
    path("categories/", expenses_view, name="expenses"),
    path("accounts/create/", create_account, name="create_account"),
    path("categories/create/", create_category, name="create_category"),
]
