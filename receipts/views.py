from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseForm, AccountForm

# Create your views here.
@login_required
def receipts_list_view(request):
    receipts = Receipt.objects.filter(purchaser=request.user)

    context = {
        "receipts": receipts,
    }
    return render(request, "receipts/list.html", context)

@login_required
def expenses_view(request):
    expenses = ExpenseCategory.objects.filter(owner=request.user)

    context = {
        "expenses": expenses,
    }
    return render(request, "receipts/expenses_list.html", context)

@login_required
def accounts_view(request):
    accounts = Account.objects.filter(owner=request.user)

    context = {
        "accounts": accounts,
    }
    return render(request, "receipts/accounts_list.html", context)

@login_required
def create_receipts(request):
    if request.method == "POST":
        form= ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser=request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)

@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseForm(request.POST)
        if form.is_valid():
            expense = form.save(False)
            expense.owner=request.user
            expense.save()
            return redirect("expenses")
    else:
        form = ExpenseForm()
    context = {
        "form": form
    }
    return render(request, "receipts/create_category.html", context)

@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner=request.user
            account.save()
            return redirect("accounts")
    else:
        form = AccountForm()
    context = {
        "form": form
    }
    return render(request, "receipts/create_account.html", context)

